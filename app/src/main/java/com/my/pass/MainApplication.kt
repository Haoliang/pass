package com.my.pass

import android.app.Application
import androidx.room.Room
import com.my.pass.data.datasources.PassDataSource
import com.my.pass.data.repositories.PassRepositoryImpl
import com.my.pass.domain.repositories.PassRepository
import com.my.pass.presentation.viewmodel.PassViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{androidContext(
            this@MainApplication)
            modules(creatureModule)
        }
    }
}

val creatureModule = module {
    single { Room.databaseBuilder(get(), PassDataSource::class.java, "pass_database").build() }
    single { get<PassDataSource>().passDao() }
    single <PassRepository>{ PassRepositoryImpl(get())}
    viewModel { PassViewModel(get()) }
}
