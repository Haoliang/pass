package com.my.pass.data.datasources

import androidx.room.*
import com.my.pass.domain.entities.Pass
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface PassDao {
    @Query("SELECT * FROM 'pass'")
    fun getAll(): Observable<List<Pass>>

    @Query("SELECT * FROM 'pass' WHERE 'id' = :id")
    fun getPassById(id: String): Observable<Pass>

    @Insert
    fun insertPass(newPost: Pass): Completable

    @Update
    fun updatePass(newPost: Pass) : Completable

    @Delete
    fun deletePass(newPost: Pass) : Completable

}