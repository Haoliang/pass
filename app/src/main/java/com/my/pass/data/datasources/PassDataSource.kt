package com.my.pass.data.datasources

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.my.pass.domain.entities.Pass
import com.my.pass.domain.entities.PassConverters

@Database(entities = [Pass::class], version = 1)
@TypeConverters(PassConverters::class)
abstract class PassDataSource : RoomDatabase() {
    abstract fun passDao() : PassDao

    companion object{
        private var INSTANCE: PassDataSource? = null

        fun getInstance(context: Context) : PassDataSource? {
            if(INSTANCE == null) {
                synchronized(PassDataSource::class) {
                    INSTANCE = databaseBuilder(context,
                        PassDataSource::class.java,
                        PassDataSource::class.java.simpleName
                        ).build()
                }
            }
            return INSTANCE;
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}