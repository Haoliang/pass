package com.my.pass.data.repositories

import android.util.Log
import com.my.pass.data.datasources.PassDao
import com.my.pass.domain.entities.Pass
import com.my.pass.domain.repositories.PassRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PassRepositoryImpl (private  val localDataSource : PassDao): PassRepository {
    override fun getPassList(): Observable<List<Pass>> {
        return localDataSource.getAll()
    }

    override fun insert(pass: Pass) {
        localDataSource.insertPass(pass)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { Log.d("PassRepo", "Insert Success") },
                { Log.d("PassRepo", "Insert Error") }
            )
    }

    override fun update(pass: Pass) {
        localDataSource.updatePass(pass)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { Log.d("RxJava", "Update Success") },
                { Log.d("RxJava", "Update Error") }
            )

    }

    override fun delete(pass: Pass) {
        localDataSource.deletePass(pass)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { Log.d("RxJava", "Delete Success") },
                { Log.d("RxJava", "Delete Error") }
            )
    }
}