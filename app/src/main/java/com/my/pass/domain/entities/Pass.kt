package com.my.pass.domain.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import java.util.*

enum class PassType {
    DAY,
    HOUR,
}
@Entity(tableName="pass")
data class Pass(
    @PrimaryKey val id:String = UUID.randomUUID().toString(),
    @ColumnInfo val type:PassType? = PassType.DAY,
    @ColumnInfo val timePeriod: Int = 0,
    @ColumnInfo val action: Boolean = false
)

class PassConverters {
    @TypeConverter
    fun passTypeToStr(type:PassType?): String? = type?.name

    @TypeConverter
    fun strToPassType(str: String?): PassType? = str?.let { PassType.valueOf(it) }
}