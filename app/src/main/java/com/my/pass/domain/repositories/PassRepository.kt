package com.my.pass.domain.repositories

import com.my.pass.domain.entities.Pass
import io.reactivex.Observable

interface PassRepository {
    fun getPassList(): Observable<List<Pass>>
    fun insert(pass: Pass)
    fun update(pass: Pass)
    fun delete(pass: Pass)
}