package com.my.pass.presentation.page

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.my.pass.R
import com.my.pass.domain.entities.Pass
import com.my.pass.domain.entities.PassType
import com.my.pass.presentation.viewmodel.PassViewModel
import com.my.pass.presentation.widget.PassListAdapter
import kotlinx.android.synthetic.main.fragment_first.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    val passViewModel: PassViewModel by viewModel()
    var passAdapter: PassListAdapter? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab.setOnClickListener { view ->
            passViewModel.insert(
                Pass(
                    type = PassType.DAY,
                    timePeriod = 2,
                    action = false
                )
            )
        }
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        pass_list.layoutManager = layoutManager
        passAdapter = PassListAdapter()
        pass_list.adapter = passAdapter
        initializeUI();
    }

    private fun initializeUI() {
        passViewModel.getPassList().observe(this, Observer<List<Pass>> {
            list ->
            val stringBuilder = StringBuilder()
            list.forEach { pass ->
                stringBuilder.append("$pass\n\n")
            }
            Log.d("Test", "data = ${stringBuilder.toString()}")
            passAdapter?.setData(list)
        })
    }
}
