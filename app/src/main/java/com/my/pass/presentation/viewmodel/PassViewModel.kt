package com.my.pass.presentation.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.my.pass.domain.entities.Pass
import com.my.pass.domain.repositories.PassRepository
import android.util.Log
import androidx.lifecycle.LiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PassViewModel(private val repositories : PassRepository) : ViewModel() {
    private val passList : MutableLiveData<List<Pass>> = MutableLiveData()

    init {
        loadPass()
    }

    fun getPassList(): LiveData<List<Pass>> {
        return passList
    }

    fun insert(pass: Pass) {
        repositories.insert(pass)
    }

    fun update(pass: Pass) {
        repositories.update(pass)
    }

    fun delete(pass: Pass) {
        repositories.delete(pass)
    }

    @SuppressLint("CheckResult")
    fun loadPass() {
        repositories.getPassList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { list -> passList.postValue(list) },
                { error -> Log.d("RxJava", "Error getting info from interactor into presenter") }
            )
    }
}