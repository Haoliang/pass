package com.my.pass.presentation.widget

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.my.pass.R
import com.my.pass.domain.entities.Pass
import com.my.pass.domain.entities.PassType

class PassListAdapter : RecyclerView.Adapter<PassViewHolder>(){
    private val datas : ArrayList<Pass>  = ArrayList()

    fun setData(list : List<Pass>) {
        datas.clear()
        datas.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.pass_item, parent, false)
        return PassViewHolder(v)
    }

    override fun getItemCount(): Int {
        return datas.size;
    }

    override fun onBindViewHolder(holder: PassViewHolder, position: Int) {
        setDataInView(holder, datas[position])
    }

    private fun setDataInView(holder: PassViewHolder, pass: Pass) {
        when(pass.type) {
            PassType.DAY -> holder.dataView.text = "Pass : ${pass.timePeriod} day"
            PassType.HOUR -> holder.dataView.text = "Pass : ${pass.timePeriod} hour"
        }
    }
}

class PassViewHolder(v: View) : RecyclerView.ViewHolder(v) {
    val dataView: TextView = v.findViewById(R.id.item_title)
}